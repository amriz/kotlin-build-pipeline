package de.amru.ideaproject.kotlin_build_pipeline

fun main(args: Array<String>) {
    val app = App()
    app.run()
}

class App {
    fun run() {
        val workingCalculator = WorkingCalculator()
        val brokenCalculator = BrokenCalculator()

        val number1 = 10
        val number2 = 20
        println("Calculate $number1 + $number2")
        println("Working calculator: " + workingCalculator.sum(number1, number2))
        println("Broken calculator:  " + brokenCalculator.sum(number1, number2))
    }
}