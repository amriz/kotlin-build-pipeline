package de.amru.ideaproject.kotlin_build_pipeline

class BrokenCalculator : Calculator {
    override fun sum(number1: Int, number2: Int): Int {
        return number1 * number2
    }
}